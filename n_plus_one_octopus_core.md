# Resolução de alguns N + 1 no [Core](https://gitlab.com/neomed/backend/octopus-core) ?

## O que é um problema N + 1 ?


A expressão "n + 1" é usada em programação para se referir a um problema comum que pode surgir quando você está trabalhando com bancos de dados **relacionais**. Esse problema surge quando você precisa buscar informações de uma tabela principal juntamente com as informações de outra tabela relacionada.

Por exemplo, imagine que você tem duas tabelas em seu banco de dados: "Users" e "Orders". Cada pedido está associado a um usuário através de uma chave estrangeira. Se você quiser buscar todos os pedidos juntamente com o nome do usuário que fez cada pedido, você pode escrever uma consulta SQL como esta:


```sql
SELECT * FROM Orders
JOIN Users ON Orders.user_id = Users.id
```

Isso irá retornar todos os pedidos juntamente com o nome do usuário correspondente. No entanto, se você precisar buscar outras informações dos usuários além do nome, você pode acabar precisando fazer uma consulta adicional para buscar essas informações para cada usuário individualmente.

Por exemplo, imagine que agora você também precisa buscar o endereço de e-mail de cada usuário. Se você tentar fazer isso usando a mesma consulta acima, você pode acabar precisando fazer uma consulta adicional para cada usuário individualmente, resultando em "n + 1" consultas:


```sql
SELECT * FROM Orders
JOIN Users ON Orders.user_id = Users.id


SELECT email FROM Users WHERE id = {user_id}
```

Isso pode se tornar um problema de desempenho se você estiver lidando com um grande número de registros, porque cada consulta individual pode levar algum tempo para ser concluída. Para evitar isso, uma solução comum é usar o conceito de ["carregamento antecipado" ou "eager loading"](https://pt.stackoverflow.com/questions/75632/o-que-%C3%A9-lazy-loading-e-eager-loading), em que você faz uma consulta única para buscar todas as informações necessárias de uma só vez, em vez de fazer consultas adicionais para cada registro individualmente.


## Como resolver N + 1 no [Core](https://gitlab.com/neomed/backend/octopus-core) ?

- Exemplo de N + 1 detectado no core

![n_plus_one](img/n_plus_one.png)

No Octopus Core, temos um serviço específico para pré-carregar dados para uma consulta GraphQL

Podemos usar a constant **PREDEFINED_PRELOADS** em nossos models para identificar dados de uma classe filha que precisam ser pré-carregados.

Exemplo retirado do model [**Exam**](https://gitlab.com/neomed/backend/octopus-core/-/blob/master/app/models/exam.rb#L6)

```ruby
  PREDEFINED_PRELOADS = [
    :ecg_exam_ai_prediction,
    :exam_files,
    :eeg_exam, :exam_type,
    :exam_type_file_extension,
    :mapa_exam_ai_extractor,
    {
      medical_report: %i[triage_ecg_medical_report]
    },
    :medical_reports,
    :operator,
    {
      patient: %i[identification_documents]
    },
    :physician_requester_user,
    :redo_template,
    :unit,
    :worklist
  ].freeze
```

- Pré-carregamento para evitar N + 1

![n_plus_one_preload](img/include_n_plus_one.png)

A classe que fornece o pré-carregamento é a [**Graphql::Queries::PreloadService**](https://gitlab.com/neomed/backend/octopus-core/-/blob/master/app/services/graphql/queries/preload_service.rb)

Este serviço permite que identifiquemos dados aninhados que precisam ser pré-carregados através do uso de **PREDEFINED_PRELOADS** e realiza o pré-carregamento.

Sobre a classe:

**queries_base?:** Verifica se o objeto resolver é uma instância da classe Queries::Base. Caso contrário, levanta uma exceção ArgumentError com uma mensagem de erro.

**predefined_preloads?:** Verifica se a classe do modelo referenciado pela query possui uma constante PREDEFINED_PRELOADS, que deve ser uma coleção de símbolos ou hashes que representam as associações a serem pré-carregadas.

**predefined_preloads:** Retorna as pré-cargas pré-definidas para o modelo referenciado pela query, caso existam.

**field_name:** Retorna o nome do campo da query resolvido por esse serviço, ou nil caso não exista.

**main_node:** Retorna o nó principal da query resolvida por esse serviço. Esse nó é representado pelo campo da query com o mesmo nome do resolver.

**nodes_node:** Retorna o nó nodes (se existir) dentro do nó principal da query. Esse nó contém os registros a serem pré-carregados.

**children_nodes_node:** Retorna um array com os símbolos que representam as associações a serem pré-carregadas. Esses símbolos são extraídos dos filhos do nó nodes.

**selected_preloads?:** Verifica se há pré-cargas pré-definidas e nós nodes na query.

**selected_preloads:** Retorna as pré-cargas selecionadas para a query, incluindo pré-cargas pré-definidas e pré-cargas personalizadas.

**select_symbol_predefined_preloads:** Retorna as pré-cargas pré-definidas representadas por símbolos que correspondem aos filhos do nó nodes.

**select_hash_predefined_preloads:** Retorna as pré-cargas pré-definidas representadas por hashes que correspondem aos filhos do nó nodes.

**hash_predefined_preload:** Retorna as associações representadas por símbolos que correspondem aos filhos do nó nodes dentro de um hash de pré-cargas pré-definidas.

**custom_preloads?:** Verifica se a classe do modelo referenciado pela query possui uma constante CUSTOM_PRELOADS, que deve ser um hash de símbolos ou procs que representam as associações a serem pré-carregadas.

**custom_preloads:** Retorna as pré-cargas personalizadas selecionadas para a query.

**selected_custom_preloads:** Retorna as pré-cargas personalizadas representadas por procs que correspondem aos filhos do nó nodes.

Com esses métodos, o serviço Graphql::Queries::PreloadService é capaz de extrair informações relevantes da query, como as associações a serem pré-carregadas, e aplicar essas pré-cargas na consulta ao banco de dados. O resultado é uma redução significativa no número de queries executadas, melhorando assim o desempenho da aplicação.

## Cuidados para evitar N + 1 em seus projetos

Use o carregamento prévio de dados (eager loading) quando possível: em vez de fazer várias consultas ao banco de dados, traga os dados necessários com uma única consulta usando métodos como includes, preload ou eager_load do ActiveRecord.

Evite chamar consultas de banco de dados dentro de loops: em vez disso, carregue todos os dados necessários antes do loop e use os dados em memória.

Evite usar associações profundas e aninhadas que carregam muitos dados: em vez disso, use joins ou includes para trazer apenas os dados necessários.

Analise suas consultas usando ferramentas como o Bullet ou o ActiveRecord::Explain para identificar problemas de n+1 em sua aplicação.

Considere usar um cache para armazenar os resultados de consultas frequentes ou caros em memória ou em disco, em vez de executá-los repetidamente.

Essas são apenas algumas das práticas que podem ajudar a evitar o problema de n+1 em suas queries. O importante é estar ciente do problema e pensar em como estruturar suas consultas para minimizar o número de chamadas desnecessárias ao banco de dados.
